# Training #

This repository is created to document the learning material for the freshers. For starters, we have markdown files for **code style**, **software design**, **unit testing** and **git**. As more topics are covered, we could keep adding to the repository.

This repository is hopefully temporary, till we get a confluence account.