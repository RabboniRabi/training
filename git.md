# GIT
## INTRODUCTION - what is git?
## CREATING A REPOSITORY
* from scratch
* converting old code to git repo
## BRANCH
* what is a branch? 
* how to create and maintain branch? 
* why do we need multiple branches?
## MERGE AND CONFLICTS
* how do we merge branches? 
* what are conflicts? 
* why do they arise? 
* how to resolve conflicts?
